#!/usr/bin/env bash

for file in bashrc bash_profile profile; do
	ln -s ~/git/dotfiles-bash/$file ~/.$file
done

mkdir -p ~/.bashrc.d
ln -s ~/git/dotfiles-bash/bashrc.d/* ~/.bashrc.d/
