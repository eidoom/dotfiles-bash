# .profile

if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

export HISTSIZE=10000
export HISTFILESIZE=10000
export HISTCONTROL="ignoreboth"
