alias rr='source ~/.bashrc'

alias l='ls --color=always'
alias l1='l -1'
alias la='l -a'
alias ll='l -lh'
alias lla='l -alh'
alias lt='l -lhtr'
alias ltt='l -lhtr | tail'

alias grep='grep --color=always'

alias tf='tail -f'

alias du='du -h'
function d() {
	du -ah --exclude="*/.*" "$@" | sort -h
}

alias df='df -h'

alias diff='diff --color=always'

alias o='xdg-open'

alias mtop='top -u$USER'
alias mhtop='htop -u$USER'

alias ipy='ipython'
alias jyc='jupyter console'

alias rs='rsync --archive --info=progress2 --partial --human-readable'

if exists wg; then
	alias wgu='sudo systemctl start wg-quick@wg0'
	alias wgd='sudo systemctl stop wg-quick@wg0'
fi

alias ddd='dd bs=8M status=progress oflag=direct'

alias ms='math -script'
function mma() {
	mathematica "$@" &
}
