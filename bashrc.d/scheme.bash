export COLOUR_SCHEME=dark

function ct(){
	if [ $COLOUR_SCHEME = "dark" ]; then
		export COLOUR_SCHEME=light
	else
		export COLOUR_SCHEME=dark
	fi
}
