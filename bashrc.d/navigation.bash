function ..() {
	if [ $# -eq 1 ] && (( $1 > 1 )); then
		cd ..
		.. $(( $1 - 1 ))
	else
		cd ..
	fi
}

alias pd='cd -'
