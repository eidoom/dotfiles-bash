if exists nvim; then
	VISUAL=nvim
elif exists vim; then
	VISUAL=vim
else
	VISUAL=vi
fi

EDITOR=$VISUAL

alias v="$VISUAL"
