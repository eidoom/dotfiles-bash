if exists dnf; then
	alias dnfu='sudo dnf update'
	alias dnfi='sudo dnf install'
	alias dnfr='sudo dnf remove'
	alias dnfl='dnf list'
	alias dnfs='dnf search'
	alias dnfq='dnf info'
	alias dnfp='dnf provides'

	alias up='dnfu -y && if exists flatpak; then flatpak update; fi'
fi
